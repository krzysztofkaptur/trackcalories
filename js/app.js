// item controller
const ItemCtrl = (function () {
	// item contructor
	const Item = function (id, name, calories) {
		this.id = id;
		this.name = name;
		this.calories = calories;
	}

	// data structure / state
	const data = {
		items: [],
		currentItem: null,
		totalCalories: 0
	}

	// public methods
	return {
		logData() {
			return data;
		},
		getItems() {
			return data.items
		},
		addItem: function (name, calories) {
			let ID;
			// Create ID
			if (data.items.length > 0) {
				ID = data.items[data.items.length - 1].id + 1;
			} else {
				ID = 0;
			}

			// Calories to number
			calories = parseInt(calories);

			// Create new item
			newItem = new Item(ID, name, calories);

			// Add to items array
			data.items.push(newItem);
			this.getTotalCalories();
			return newItem;
		},
		getTotalCalories() {
			let total = 0;
			data.items.forEach(item => {
				total += parseInt(item.calories);
			})
			return total;
		},
		// get item with id
		getItemById(id) {
			let targetArr = data.items.filter(item => item.id === id);
			let target = targetArr[0];
			
			data.currentItem = target;

			UICtrl.updateInputs();
		},
		deleteItems(id) {
			// new array
			let newItems = data.items.filter(item => item.id !== id);

			data.items = newItems;
		},
		updateItems(id, input) {
			let item = data.items.filter(item => item.id === id);

			if (input.name !== '' && input.calories !== '') {
				// change item
				item[0].name = input.name;
				item[0].calories = input.calories;
			}
		}
	}
})();

// ui controller
const UICtrl = (function () {
	const UISelectors = {
		totalHeading: document.querySelector('.total-heading'),
		itemList: '#item-list',
		addBtn: document.querySelector('.add-btn'),
		updateBtn: document.querySelector('.update-btn'),
		deleteBtn: document.querySelector('.delete-btn'),
		backBtn: document.querySelector('.back-btn'),
		itemName: document.querySelector('#item-name'),
		itemCalories: document.querySelector('#item-calories'),
		totalCalories: document.querySelector('.total-calories'),
		editItemBtn: document.querySelectorAll('.edit-item')
	}


	// public methods
	return {
		makeListItem(item) {
			document.querySelector(UISelectors.itemList).classList.add('collection');

			const template = `
			<li class="collection-item" id="item-${item.id}">
				<strong>${item.name}: </strong> <em>${item.calories}</em>
				<a href="#" class="secondary-content">
					<i class="edit-item fa fa-pencil"></i>
				</a>
			</li>`;
			return template;
		},
		populateItemList(items) {
			let template = '';
			items.forEach(item => {
				template += this.makeListItem(item);
			});

			document.querySelector(UISelectors.itemList).innerHTML = template;
			this.insertTotalCalories();
		},
		getItemInput() {
			return {
				name: UISelectors.itemName.value,
				calories: UISelectors.itemCalories.value
			}
		},
		getSelectors() {
			return UISelectors;
		},
		addItemToUI(item) {
			document.querySelector(UISelectors.itemList).innerHTML += this.makeListItem(item);
			this.clearInput();
			this.insertTotalCalories();
			this.clearEditState();
		},
		clearInput() {
			UISelectors.itemName.value = '';
			UISelectors.itemCalories.value = '';
		},
		insertTotalCalories() {
			UISelectors.totalCalories.textContent = ItemCtrl.getTotalCalories();
		},
		clearEditState() {
			this.clearInput();
			UISelectors.updateBtn.style.display = 'none';
			UISelectors.deleteBtn.style.display = 'none';
			UISelectors.backBtn.style.display = 'none';
			UISelectors.addBtn.style.display = 'block';
		},
		updateInputs() {
			const currentItem = ItemCtrl.logData().currentItem;
			
			UISelectors.itemName.value = currentItem.name;
			UISelectors.itemCalories.value = currentItem.calories;

			UISelectors.updateBtn.style.display = 'inline-block';
			UISelectors.deleteBtn.style.display = 'inline-block';
			UISelectors.backBtn.style.display = 'inline-block';
			UISelectors.addBtn.style.display = 'none';
		}
	}
})();

// app controller
const App = (function (ItemCtrl, UICtrl) {

	const loadEventListeners = () => {
		// get ui selectors
		const UISelectors = UICtrl.getSelectors();

		// add item event
		UISelectors.addBtn.addEventListener('click', itemAddSubmit);

		document.querySelector(UISelectors.itemList).addEventListener('click', itemUpdateSubmit);

		// update item
		UISelectors.updateBtn.addEventListener('click', updateItems);

		// go back
		UISelectors.backBtn.addEventListener('click', goBack);

		// delete item
		UISelectors.deleteBtn.addEventListener('click', itemDeleteSubmit);

		// disable enter
		document.addEventListener('keypress', (e) => {
			if(e.keyCode == 13) {
				e.preventDefault();
			}
		});
	}

	// add item submit
	function itemAddSubmit(e) {
		e.preventDefault();
		ItemCtrl.getItems();

		// get form input from ui controller
		const input = UICtrl.getItemInput();

		// check for name and calorie input
		if (input.name !== '' && input.calories !== '') {
			if (!isNaN(input.calories)) {
				// add item
				const newItem = ItemCtrl.addItem(input.name, input.calories);

				// add item to ui
				UICtrl.addItemToUI(newItem);
			}
		}
	}

	// find item in items
	function itemUpdateSubmit(e) {
		e.preventDefault();

		let id = 0;
		// get id
		if (e.target.classList.contains('edit-item')) {
			id = parseInt(e.target.parentElement.parentElement.id.slice(5));
		}
		// get item
		const itemToEdit = ItemCtrl.getItemById(id);
	}

	function updateItems(e) {
		e.preventDefault();

		// get it's id
		const id = ItemCtrl.logData().currentItem.id;		

		// get new inputs
		const input = UICtrl.getItemInput();

		// find this element by id
		// let item = ItemCtrl.logData().items.filter(item => item.id === id);
		ItemCtrl.updateItems(id, input);

		// update UI
		UICtrl.populateItemList(ItemCtrl.logData().items);

		// clear edit state
		UICtrl.clearEditState();

	}

	// delete item
	function itemDeleteSubmit(e) {
		e.preventDefault();

		// get it's id
		const id = ItemCtrl.logData().currentItem.id;

		// update items
		ItemCtrl.deleteItems(id);

		// update UI
		UICtrl.populateItemList(ItemCtrl.logData().items);

		// clear edit state
		UICtrl.clearEditState();
	}

	// go back
	function goBack(e) {
		e.preventDefault();

		UICtrl.clearEditState();
	}

	// public methods
	return {
		init: function () {
			// clear edit state
			UICtrl.clearEditState();

			// fetch items from data structure
			const items = ItemCtrl.getItems();

			// populate list with items
			UICtrl.populateItemList(items);

			// load event listeners
			loadEventListeners();

		}
	}

})(ItemCtrl, UICtrl);

// init app
App.init();